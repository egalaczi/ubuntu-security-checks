#!/bin/bash

txtund=$(tput sgr 0 1)          # Underline
txtbld=$(tput bold)             # Bold
bldred=${txtbld}$(tput setaf 1) #  red
bldblu=${txtbld}$(tput setaf 4) #  blue
bldwht=${txtbld}$(tput setaf 7) #  white
bldgrn=${txtbld}$(tput setaf 2) #  white
txtrst=$(tput sgr0)             # Reset
info=${bldwht}*${txtrst}        # Feedback
pass=${bldblu}*${txtrst}
warn=${bldred}*${txtrst}
ques=${bldblu}?${txtrst}

#Prints horizontal line
hl="./util/hl.sh"


FAILED=0
PASSED=0
$hl
for i in ./scripts/*
do
    $hl
    $i
    if [ $? == 1 ]; then
        echo $bldred"FAIL" $txtrst
        ((FAILED++))
    else
        echo $bldgrn"PASS" $txtrst
        ((PASSED++))
    fi   
done

$hl
if [ $FAILED == 0 ]; then
    echo $bldgrn"Passed $PASSED of "$(($PASSED+$FAILED)) $txtrst
else
    echo $bldred"Passed $PASSED of "$(($PASSED+$FAILED)) $txtrst
fi



