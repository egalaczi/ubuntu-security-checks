#!/bin/bash
echo Checking ssh port...
PORT=`grep -e "^Port " /etc/ssh/sshd_config | cut -d \  -f 2`
if [ $PORT == 22 ]; then
	printf "Ssh server is listening on port 22, this is discouraged. \n"
        printf "Please set a different port in /etc/ssh/sshd_config! Example: 22789, 22789 etc.\n"
	exit 1
else
	exit 0
fi
