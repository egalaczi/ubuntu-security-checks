#!/bin/bash
echo Checking if ssh allows PasswordAuthentication...
if  grep -q -e "^[#]*PasswordAuthentication yes" /etc/ssh/sshd_config 
then
    echo Password login is enabled for ssh. It is encouraged to only use key based authentication.
    echo Auth logs show `grep "Failed password" /var/log/auth.log | wc -l` failed password attempts.
    exit 1
else
    exit 0
fi

