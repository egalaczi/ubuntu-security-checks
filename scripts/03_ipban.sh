#!/bin/bash
echo Checking for suspicious host banning...

echo "Suspicious hosts with failed password attempts(Count, Ip):"
#Get ip of failed password attempts, sort and count occurences, 
#remove leading spaces and sort by most frequent finally show 10 most common suspicious ip.
grep "Failed password" /var/log/auth.log | grep -oe "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | sort | uniq -c | sed -e 's/^[ \t]*//' | sort -rgk 1 | head -n 10

sudo service denyhosts status
if [ $? != 0 ]; then
	sudo service fail2ban status
	if [ $? != 0 ]; then
		printf "Neither denyhosts or fail2ban is running\n"
		printf "Please consider installing either of the ip banning services.\n"
		exit 1
	else
		echo "Found running fail2ban service"
		exit 0
	fi
else
	echo "Found running denyhosts service"
	exit 0
fi
 
