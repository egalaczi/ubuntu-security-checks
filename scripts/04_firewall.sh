#!/bin/bash
echo Checking firewall

ufw version
if [ $? == 127 ]; then
  echo "Please install ufw firewall tool."
  exit 1
else
  active=`sudo ufw status | grep 'Status: active' | wc -l`
  if [ $active == 1 ]; then
    sudo ufw status | cut -d \  -f 1 | grep -oe "[0-9]*" | sort | uniq > ufwout
    sort ports.config -o ports.config
    result=`comm -31 ports.config ufwout | wc -l`
    if [[ $result == 0 ]]; then
  	exit 0
    else 
        echo "Extra ports are open:"
  	comm -31 ports.config ufwout
  	exit 1
    fi 
  else
    echo "ufw is not enabled"
    exit 1
  fi 
fi

