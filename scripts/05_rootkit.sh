#!/bin/bash
echo Checking for rootkits...
chkrootkit -V
if [ $? != 127 ]; then
	sudo chkrootkit -q 2> .rootkit.log
	cat .rootkit.log
	CNT=`cat .rootkit.log | wc -l`
	echo $CNT
	if [ $CNT == 0 ]; then
		exit 0
	else
		echo "Chkrootkit found some potential problems."
		exit 1
	fi
else
	echo Chkrootkit is not installed, Chkrootkit is recommended.
	exit 1 
fi



