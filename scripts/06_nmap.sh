#!/bin/bash
echo Checking open ports

nmap localhost | grep -e "^[0-9]*/tcp" -e "^[0-9]*/udp" | grep open | cut -d \/ -f 1 | sort -o nmapout
sort ports.config -o ports.config
result=`comm -31 ports.config nmapout | wc -l`
if [[ $result == 0 ]]; then
  exit 0
else 
  echo "Extra ports are open:"
  comm -31 ports.config nmapout
  exit 1
fi
