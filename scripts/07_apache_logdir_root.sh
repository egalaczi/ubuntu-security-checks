#!/bin/bash
echo Checking that apache log dir is owned by root...
dir=`grep APACHE_LOG_DIR /etc/apache2/envvars | tr '=' '$' | cut -d \$ -f 2`
owners=`ls -al $dir | grep -e "^[d|-]"| cut -s -d \  -f 3 | uniq`
if [ $owners == 'root' ]; then
   if [ $(ls -al $dir | grep -e "^........w." | wc -l) == 0 ]; then
 	exit 0
   else
	echo "Contents of $dir should not be other writable!"
        exit 1
   fi
else
   echo "Only root should be owner of $dir and contents!"
fi
