#!/bin/bash
echo Checking if apache and OS version is public...
result=`grep -e '^ServerTokens ProductOnly' -e '^ServerSignature Off' /etc/apache2/apache2.conf | wc -l`
if [ $result == 2 ]; then
  exit 0
else
  printf "Apache and/or OS version will be readable in response headers, this is discouraged \n"
  printf "Please set the following in /etc/apache2/apache2.conf \n"
  printf "ServerTokens ProductOnly \n ServerSignature Off \n"
  exit 1
fi
