#!/bin/bash
echo Checking that apache user is not priviledged...

USER=`cat /etc/apache2/envvars | grep APACHE_RUN_USER | cut -d \= -f 2`
USER2=`cat /etc/apache2/apache2.conf | grep -e "^User" | cut -d \  -f 2`
result=`sudo cat /etc/sudoers | grep -e "^$USER" -e "^$USER2" | wc -l`
if [ result == 0 ]; then 
	exit 0
else
	echo "Apache runs as user $USER who is a sudoer, this is discouraged."
	exit 1
fi
