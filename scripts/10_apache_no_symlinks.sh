#!/bin/bash
echo Checking if any sites enable FollowSymlinks option...
for i in /etc/apache2/sites-enabled/*; do 
	cnt=`cat $i | grep -i -e '[\ |+]FollowSymlinks'  | wc -l`
	if [ $cnt != 0 ]; then
		echo "Some directives in $i enable FollowSymlinks. This is discouraged."
		exit 1
	else
		exit 0
	fi
done
